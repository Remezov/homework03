package Game;

import java.util.Scanner;
import java.util.Random;

public class App {

    public static void main(String[] args) {
        start();
    }

    private static void start() {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        int hiddenNumber = random.nextInt(101);
        int result = -1;
        System.out.println("Do you want to play? \n 1 - Yes  2 - No");
        int choice = scanner.nextInt();
        if (choice == 1) {
            result = firstAttempt(scanner, hiddenNumber);
            if (hiddenNumber == result)
                System.out.println("You win!");
            else
                System.out.println("You lose!");
        }
        System.out.println("Game over!");
    }

    private static int firstAttempt(Scanner scanner, int hiddenNumber) {
        int delta = 0;
        System.out.println("Give me a number from 0 to 100");
        int enterNumber = scanner.nextInt();
        if ((enterNumber < 0) || (enterNumber > 100)) {
            System.out.println("Incorrect number!");
            return continuationOfGame(scanner, enterNumber, hiddenNumber, delta);
        }
        if (hiddenNumber != enterNumber) {
            if (hiddenNumber > enterNumber) {
                if ((hiddenNumber - enterNumber) <= 5)
                    System.out.println("Hot!");
                else {
                    System.out.println("Cold!");
                    delta = hiddenNumber - enterNumber;
                }
            } else {
                if ((enterNumber - hiddenNumber) <= 5)
                    System.out.println("Hot!");
                else {
                    System.out.println("Cold!");
                    delta = hiddenNumber - enterNumber;
                }
            }
            return continuationOfGame(scanner, enterNumber, hiddenNumber, delta);
        }
        return enterNumber;
    }

    private static int continuationOfGame(Scanner scanner, int enterNumber, int hiddenNumber, int delta) {
        while (hiddenNumber != enterNumber) {
            System.out.println("Continue? \n 1 - Yes  2 - No");
            int choice = scanner.nextInt();
            if (choice != 1) {
                break;
            }
            System.out.println("Give me a number from 0 to 100");
            enterNumber = scanner.nextInt();
            if ((enterNumber < 0) || (enterNumber > 100)) {
                System.out.println("Incorrect number!");
                continue;
            }
            if (hiddenNumber != enterNumber) {
                if (hiddenNumber > enterNumber) {
                    if (delta > (hiddenNumber - enterNumber)) {
                        System.out.println("Warmer!");
                        delta = hiddenNumber - enterNumber;
                    } else {
                        System.out.println("Cold!");
                        delta = hiddenNumber - enterNumber;
                    }
                } else {
                    if (delta > (enterNumber - hiddenNumber)) {
                        System.out.println("Warmer!");
                        delta = enterNumber - hiddenNumber;
                    } else {
                        System.out.println("Cold!");
                        delta = enterNumber - hiddenNumber;
                    }
                }
            }
        }
        return enterNumber;
    }
}